-- remove any previously existing tables
DROP TABLE IF EXISTS CheckIns;
DROP TABLE IF EXISTS QRInstances;

/* Creates table for all check ins
*/
CREATE TABLE CheckIns(
    checkin_id INTEGER AUTO_INCREMENT NOT NULL,
    unix_one INTEGER NOT NULL,
    unix_two INTEGER NOT NULL,
    course_code VARCHAR(50) NOT NULL,
    teacher VARCHAR(50) NOT NULL,
    student INTEGER NOT NULL,
    hash_string VARCHAR(500) NOT NULL,

    PRIMARY KEY (checkin_id)
);

/* Creates table for all the QR instances
*/
CREATE table QRInstances(
    instance_id INTEGER auto_increment NOT NULL,
    qr_url varchar(200) NOT NULL,
    unix_one INTEGER NOT NULL,
    teacher varchar(50) NOT NULL,
    course_code VARCHAR(50),
    max_count INTEGER NOT NULL,
    hash_string VARCHAR(500) NOT NULL,

    PRIMARY KEY (instance_id)
);
