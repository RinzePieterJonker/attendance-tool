/**
 * Checks the current count of students that have checked in and displays it in real-time.
 */
$(document).ready(function() {
    var studentcounter = document.getElementById("studentcounter");
    // gets count from servlet that's connected to database
    var get_count = function () {
        $.when(
            $.get("counter",{hash: document.getElementById("hash").value}, function (data) {
                count = data;
            })
        ).then(function () {
            // last count gets replaced by current count
            studentcounter.innerHTML = parseInt(count, 10);
        })


    };
    // refreshes the get_count function every time interval
    window.setInterval(function () {
        get_count();
    }, 500); //in milliseconds
});