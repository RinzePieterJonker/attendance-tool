/**
 * Searches the URL for parameters and puts them in hidden fields.
 */
$(document).ready(function () {
    // grabs all parameters from URL query string
    var getUrlParameter = function getUrlParameter(param) {
        var pageURL = window.location.search.substring(1);
        var uRLVariables = pageURL.split('&');
        var paramName;
        var i;

        for (i = 0; i < uRLVariables.length; i++) {
            paramName = uRLVariables[i].split('=');

            if (paramName[0] === param) {
                return paramName[1] === undefined ? true : decodeURIComponent(paramName[1]);
            }
        }
    };
    // found parameters are put in variables
    var teacherCode = getUrlParameter('teachercode');
    var courseCode = getUrlParameter('coursecode');
    var unix1 = getUrlParameter('unixdate1');
    var hashString = getUrlParameter("hashstring");

    // vars are put in the hidden input fields
    document.getElementById('hidden_teacher_code').value = teacherCode;
    document.getElementById('hidden_course_code').value = courseCode;
    document.getElementById('hidden_unixdate1').value = unix1;
    document.getElementById("hidden_hashstring").value = hashString;

});