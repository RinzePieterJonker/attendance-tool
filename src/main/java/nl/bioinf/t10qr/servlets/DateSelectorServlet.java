package nl.bioinf.t10qr.servlets;

import nl.bioinf.t10qr.dao.DatabaseException;
import nl.bioinf.t10qr.dao.MyAppDao;
import nl.bioinf.t10qr.dao.MyAppDaoMySQL;
import nl.bioinf.t10qr.db_utils.DbUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * a servlet that gets all the unixdates for a given course
 *
 * Copyright (c) 2018 Rinze-Pieter Jonker
 * All rights reserved
 */
@WebServlet(name = "DateSelectorServlet", urlPatterns = "/day")
public class DateSelectorServlet extends HttpServlet {
    private MyAppDao dao;

    @Override
    public void init() throws ServletException {
        super.init();
        dao = new MyAppDaoMySQL();
        try {
            DbUser user = new DbUser(getServletContext().getInitParameter("sqlHost"),
                    getServletContext().getInitParameter("sqlDatabase"),
                    getServletContext().getInitParameter("sqlUser"),
                    getServletContext().getInitParameter("sqlPassword"));
            dao.connect(user);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void destroy() {
        super.destroy();
        try {
            dao.disconnect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleRequest(request, response);
    }

    /**
     * handles the code for both the get and post method
     * @param request
     * @param response
     */
    private void handleRequest(HttpServletRequest request, HttpServletResponse response) {
        String course = request.getParameter("course");
        String courses = JobRunner(course);

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        try {
            response.getWriter().print(courses);
        } catch (IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Runs the Mysql function and deals with the Exceptions
     * @param course name of the course (string)
     * @return String with all the hashstrings and dates
     */
    private String JobRunner(String course) {
        String result = "";
        try {
            result = dao.getAllDates(course);
        } catch (DatabaseException ex) {
            ex.printStackTrace();
        }
        return result;

    }
}
