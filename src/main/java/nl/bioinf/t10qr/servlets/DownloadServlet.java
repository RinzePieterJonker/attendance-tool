package nl.bioinf.t10qr.servlets;


import nl.bioinf.t10qr.dao.DatabaseException;
import nl.bioinf.t10qr.dao.MyAppDao;
import nl.bioinf.t10qr.dao.MyAppDaoMySQL;
import nl.bioinf.t10qr.db_utils.DbUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.ArrayList;

/**
 * Servlet that handles all the download requests
 *
 * Copyright (c) 2018 Rinze-Pieter Jonker
 * All rights reserved
 */
@WebServlet(name = "DownloadServlet", urlPatterns = "/download.do")
public class DownloadServlet extends HttpServlet {
    private MyAppDao dao;

    @Override
    public void init() throws ServletException {
        super.init();
        dao = new MyAppDaoMySQL();
        try {
            DbUser user = new DbUser(getServletContext().getInitParameter("sqlHost"),
                    getServletContext().getInitParameter("sqlDatabase"),
                    getServletContext().getInitParameter("sqlUser"),
                    getServletContext().getInitParameter("sqlPassword"));
            dao.connect(user);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void destroy() {
        super.destroy();
        try {
            dao.disconnect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(true);
        String sessionId = session.getId();

        ArrayList<String> dataArray = new ArrayList<String>();

        String hashString = assignHashString(request);
        try {
            dataArray = dao.DatabaseDump(hashString);
        } catch (DatabaseException ex) {
            ex.printStackTrace();
        }

        String outmap = "AttendenceTool/";

        File file = new File(getServletContext().getInitParameter("output.map") + outmap);

        if (!file.exists()){
            try {
                file.mkdir();
            } catch (SecurityException se){
                se.printStackTrace();
            }
        }

        String fileName = getServletContext().getInitParameter("output.map") + outmap  + sessionId + ".csv";

        File result = getFile(fileName, dataArray);

        response.setContentType("text/plain; charset=UTF-8");
        response.setContentLength((int) result.length());
        response.setHeader( "Content-Disposition",
                String.format("attachment; filename=\"%s\"", result.getName()));

        /*puts the file into the output stream*/
        if(result.length() < 4096){
            OutputStream out = response.getOutputStream();
            try (FileInputStream in = new FileInputStream(result)) {
                byte[] buffer = new byte[4096];
                int length;
                while ((length = in.read(buffer)) > 0) {
                    out.write(buffer, 0, length);
                }
                out.flush();
            }
        }

    }

    /**
     * function that checks if the user wants to only download one class or if he wants to download all the information
     * @param request HttpServletRequest object
     * @return hashString a string with the given hashstring or if the user wants to download everything 'NVT'
     */
    private String assignHashString(HttpServletRequest request) {
        boolean checkBox = request.getParameter("amount") != null;
        String hashString;
        if (checkBox) {
            hashString = "NVT";
        } else {
            hashString = request.getParameter("hashString");
        }
        return hashString;

    }

    /**
     * function that will make the download file for the given data
     * @param outFile the location of where you want to store the data
     * @param dataArray a String array with all the rows from the database
     * @return the File object of the dataArray
     * @throws IOException
     */
    private File getFile(String outFile, ArrayList<String> dataArray) throws IOException{
        FileWriter writer = new FileWriter(outFile);
        int size = dataArray.size();

        for (int i =0; i < size; i++) {
            String string = dataArray.get(i);
            writer.write(string);
            if (i < size - 1) {
                writer.write("\n");
            }
        }
        writer.close();
        return new File(outFile);
    }
}

