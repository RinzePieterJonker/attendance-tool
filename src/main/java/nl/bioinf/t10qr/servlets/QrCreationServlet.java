package nl.bioinf.t10qr.servlets;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;


/**
 * this servlet will send all the needed data to the QRpage.jsp
 *
 * Copyright (c) 2018 Rinze-Pieter Jonker
 * All rights reserved
 */
@WebServlet(name = "QrCreationServlet", urlPatterns = "/create.do")
public class QrCreationServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleParameters(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleParameters(request, response);
    }

    /**
     * handles both the doPost and doGet
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void handleParameters(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String teacher = request.getParameter("Teacher");
        String course = request.getParameter("subject");
        String maxCount = request.getParameter("maxcount");
        String time = request.getParameter("unixdate");

        String webURL = getServletContext().getInitParameter("website_url");

        request.setAttribute("unixdate", time);
        request.setAttribute("teachercode", teacher);
        request.setAttribute("coursecode", course);
        request.setAttribute("maxcount", maxCount);
        request.setAttribute("websiteURL", webURL);

        String randomString = createRandom();

        request.setAttribute("hashString", randomString);

        RequestDispatcher view = request.getRequestDispatcher("/QRpage.jsp");
        view.forward(request, response);
    }

    /**
     * creates a hashstring
     * @return hashstring
     */
    private String createRandom() {
        String randomString = UUID.randomUUID().toString();
        return randomString;
    }
}
