package nl.bioinf.t10qr.servlets;

import nl.bioinf.t10qr.DatabaseInstance.DatabaseInstance;
import nl.bioinf.t10qr.dao.DatabaseException;
import nl.bioinf.t10qr.dao.MyAppDao;
import nl.bioinf.t10qr.dao.MyAppDaoMySQL;
import nl.bioinf.t10qr.db_utils.DbUser;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Servlet that will send all the needed data to the Results.jsp page
 *
 * Copyright (c) 2018 Rinze-Pieter Jonker
 * All rights reserved
 */
@WebServlet(name = "StatisticsServlet", urlPatterns = "/resultpage.do")
public class StatisticsServlet extends HttpServlet {
    private MyAppDao dao;

    @Override
    public void init() throws ServletException {
        super.init();
        dao = new MyAppDaoMySQL();
        try {
            DbUser user = new DbUser(getServletContext().getInitParameter("sqlHost"),
                    getServletContext().getInitParameter("sqlDatabase"),
                    getServletContext().getInitParameter("sqlUser"),
                    getServletContext().getInitParameter("sqlPassword"));
            dao.connect(user);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void destroy() {
        super.destroy();
        try {
            dao.disconnect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int studentCount = 0;
        long unixTime = Integer.parseInt(request.getParameter("ti"));

        try {
            studentCount = dao.countCheckIns(request.getParameter("hs"));
        } catch (DatabaseException ex) {
            ex.printStackTrace();
        }

        ProcessBuilder processBuilder = GetProcessBuilder();
        ArrayList<DatabaseInstance> qrInstances = JobRunner(processBuilder);

        ArrayList<String> teachers = new ArrayList<String>();

        try {
            teachers  = dao.getAllTeachers();
        } catch ( DatabaseException ex) {
            ex.printStackTrace();
        }

        request.setAttribute("teachers", teachers);
        request.setAttribute("count", studentCount);
        request.setAttribute("hashstring", request.getParameter("hs"));
        request.setAttribute("teacher", request.getParameter("te"));
        request.setAttribute("unixdate1", request.getParameter("ti"));
        request.setAttribute("course", request.getParameter("co"));
        request.setAttribute("date", GetDateFromUnix(unixTime));
        request.setAttribute("QRinstances", qrInstances);

        RequestDispatcher view = request.getRequestDispatcher("/Results.jsp");
        view.forward(request, response);
    }

    /**
     * runs the job for the database
     * @param processBuilder
     * @return
     */
    private ArrayList JobRunner(ProcessBuilder processBuilder) {

        ArrayList<DatabaseInstance> instanceList = new ArrayList<DatabaseInstance>();

        try {
            Process process = processBuilder.start();
            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(process.getInputStream()));

            String s;
            int line = 0;

            while ((s = stdInput.readLine()) != null){
                if (line != 0) {
                    String[] lineParts = s.split("\t");
                    String unixTime = GetDateFromUnix(Integer.parseInt(lineParts[0]));
                    instanceList.add(new DatabaseInstance(lineParts[1], unixTime, lineParts[2]));
                }
                line += 1;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return instanceList;
    }

    /**
     * Builds the Process for the JobRunner
     * @return ProcessBuilder object
     */
    private ProcessBuilder GetProcessBuilder() {
        String[] command = new String[] {"mysql", "-e", "SELECT unix_one, hash_string, teacher FROM QRInstances"};
        return new ProcessBuilder(command);
    }

    /**
     * Function that translates to unixtimestamps to a normal timestamp
     * @param unixTime (Long) unixtimestamp
     * @return The normal date
     */
    private String GetDateFromUnix(long unixTime) {

        Date date = new Date(unixTime*1000L);

        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT+1"));

        return sdf.format(date);
    }
}


