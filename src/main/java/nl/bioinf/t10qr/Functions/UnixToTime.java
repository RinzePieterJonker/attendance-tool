package nl.bioinf.t10qr.Functions;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class UnixToTime {
    public static void main(String[] args) {
        System.out.println(UnixToTime.GetDateFromUnix(1547561475));
    }

    public static String GetDateFromUnix(long unixTime) {

        Date date = new Date(unixTime * 1000L);

        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT+1"));

        return sdf.format(date);
    }
}
